﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog_2.Models
{
    public class Account
    {
        [Required(ErrorMessage = "ko được để trống")]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [DataType(DataType.EmailAddress)]
        public string Email { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(50, ErrorMessage = "so ky tu tối đa 50")]
        public string FirstName { set; get; }

        [Required(ErrorMessage = "ko được để trống")]
        [StringLength(50, ErrorMessage = "so ky tu tối đa 50")]
        public string LastName { set; get; }

        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ID { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}