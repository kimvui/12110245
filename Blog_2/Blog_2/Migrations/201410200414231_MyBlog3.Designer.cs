// <auto-generated />
namespace Blog_2.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class MyBlog3 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(MyBlog3));
        
        string IMigrationMetadata.Id
        {
            get { return "201410200414231_MyBlog3"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
