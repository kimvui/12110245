namespace Blog_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MyBlog1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Body", c => c.String(nullable: false, maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViet", "Body", c => c.String(maxLength: 250));
        }
    }
}
